FROM registry.gitlab.com/texttoconvert/tools/python-pandoc-texlive:latest

# Set the working directory to /builder
WORKDIR /app

COPY . /app

RUN ./builder/build_website.sh
