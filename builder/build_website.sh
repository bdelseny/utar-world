#!/bin/bash

BOOKS_PATH="./books"
IMAGES_PATH="./images"
COMMON_CHAR_PATH=${BOOKS_PATH}"/commun/personnages"
COMMON_CHAR_FILE="commun_personnages"
PUBLIC_PATH="./public"
INDEX_FILE="index.html"
NAVBAR_FILE="navbar.html"
STYLE_FOLDER="./style"
PDF_PATH="./pdfPath"
MISC="./characterSheet"

rm -rf ${PUBLIC_PATH}
mkdir ${PUBLIC_PATH}
cp -r ./${STYLE_FOLDER} ${PUBLIC_PATH}/
cp -r ${IMAGES_PATH} ${PUBLIC_PATH}/

pandoc ${BOOKS_PATH}/index.md --from=markdown --to=html --output=${PUBLIC_PATH}/${INDEX_FILE}
pandoc ${BOOKS_PATH}/navbar.md --from=markdown --to=html --output=${PUBLIC_PATH}/${NAVBAR_FILE}
echo -e "<nav>\n<input type='checkbox' style='display:none;' id='menu-toggle'/>\n<label id='label-menu' for='menu-toggle'><span id='rotator'>≡</span></label>\n<div id='menu-to-display'>\n$(cat ${PUBLIC_PATH}/${NAVBAR_FILE})\n</div>\n</nav>" > "${PUBLIC_PATH}/${NAVBAR_FILE}"

rm -rf $PDF_PATH
mkdir $PDF_PATH

for mdFile in ${COMMON_CHAR_PATH}/*.md
do
    { printf "\\\newpage\n\n"; cat ${mdFile};} > ${PDF_PATH}/$(basename -- "${mdFile}").new
done

pandoc ${PDF_PATH}/*.new --from=markdown --to=pdf --toc -o "${PUBLIC_PATH}/commun_personnages.pdf" -s --metadata=title:"Les mondes d'UTAR: Personnages" -f markdown-implicit_figures

pandoc ${COMMON_CHAR_PATH}/*.md --toc -o ${PUBLIC_PATH}/${COMMON_CHAR_FILE}.md -s
pandoc ${PUBLIC_PATH}/${COMMON_CHAR_FILE}.md -o ${PUBLIC_PATH}/${COMMON_CHAR_FILE}.html
echo -e "<div id='main'>\n$(cat ${PUBLIC_PATH}/${COMMON_CHAR_FILE}.html)\n</div>" > ${PUBLIC_PATH}/${COMMON_CHAR_FILE}.html
pandoc ${PUBLIC_PATH}/${COMMON_CHAR_FILE}.html --metadata=title:"Les mondes d'UTAR: Personnages" --table-of-contents\
    --from=html --to=html --output=${PUBLIC_PATH}/${COMMON_CHAR_FILE}.html -s -B "${PUBLIC_PATH}/${NAVBAR_FILE}" \
    --metadata=css:"./style/main.css"

echo -e "<div id='main'>\n$(cat ${PUBLIC_PATH}/${INDEX_FILE})\n</div>" > "${PUBLIC_PATH}/${INDEX_FILE}"
pandoc "${PUBLIC_PATH}/${INDEX_FILE}" --metadata=title:"Les mondes d'UTAR" \
    --from=html --to=html --output="${PUBLIC_PATH}/${INDEX_FILE}" -s -B "${PUBLIC_PATH}/${NAVBAR_FILE}" \
    --metadata=css:"./style/main.css"

cp ${MISC}/** ${PUBLIC_PATH}
