# UTAR World

UTAR world is a Role-play game. \
*Only in french for start*

## Local build

For a local HTML build and deployment in docker use the following:

```sh
docker-compose down -v # to remove all docker-compose created volumes
docker-compose up --build --force-recreate # to force images build
```
