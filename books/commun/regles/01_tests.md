# Jets de tests

Tous les jets, de combats ou de compétences, fonctionnent par seuils de réussite.

## Seuils de réussite

| Type de seuil     | Réussite Critique | Réussite Majeure                             | Réussite Normale | Échec | Échec Critique |
| ----------------- | ----------------- | -------------------------------------------- | ---------------- | ----- | -------------- |
| Valeur au dé 100 | ⩽ 3               | ⩽ 1/2 VC^[Valeur de combat ou de compétence] | ⩽ VC             | > VC  | ⩾ 98           |

## Combats

Lors de combats la Réussite Critique est une réussite obligatoire.
À l'inverse un Échec Critique est un échec obligatoire.

### Échec Critique

#### Échec critique en attaque

L'Échec Critique lors d'une attaque fait perdre un tour au personnage.

Lors d'une attaque au CAC^[Corps à corps] *précise* l'Échec Critique met le joueur en position difficile, augmentant la difficulté de défense d'un seuil de dés.

Lors d'une attaque à distance *précise* l'Échec Critique fait toucher un allié comme une réussite normale, celle-ci ne peut être esquivée.

Lors d'une attaque magique *précise* l'Échec Critique fait renverser le sort sur le lanceur, il en va de même pour les reliqueurs.

Lors d'un bonus *précis* l'Échec Critique inverse l'effet, inversement pour un malus.

#### Échec critique en défense

L'Échec Critique lors d'une esquive fait tomber le joueur, le rendant vulnérable jusqu'à son prochain tour.
Il peut être relevé par un allié, ce qui prend une action, ce qui prend une action à l'allier.
Si personne ne le relève d'ici son tour de jeu, il perd son tour de jeu pour se relever.
Une fois relevé le jour peut à nouveau se défendre.

L'Échec Critique lors d'une parade fait perdre son arme au joueur, il ne peut plus parer jusqu'à ce qu'il ramasse son arme.
Un allié peut lui ramasser et lui rendre son arme, ce qui prend une action à l'allier.
Si personne ne lui rend son arme d'ici son tour de jeu, il peut perdre son tour de jeu pour ramasser son arme.

L'Échec Critique lors d'un contre fait monter l'attaque de son opposant en Réussite Critique, cela n'a aucun effet si cela était déjà une réussite critique.

### Réussite Critique

#### Réussite critique en attaque

La réussite critique double les dégâts d'une attaque ou le nombre de tours d'effet d'un sort.

Une réussite critique ne peut être contrée^[esquivée ou parée] que par une réussite critique.
Si elle est contrée le joueur dispose d'un deuxième tour de jeu à la fin de la manche.

#### Réussite critique en défense

La réussite critique donne un avantage de positionnement.
À la prochaine manche le joueur joue en premier puis à son tour normal.

### Paliers de réussite

Les combats fonctionnent par paliers de réussite.

Une meilleure réussite l'emporte sur une moins bonne réussite :

- Réussite critique > Réussite Majeure > Réussite Normale > Échec > Échec Critique

Ainsi si deux personnages sont face à face :

- L'attaquant fait une Réussite Majeure
- Le défenseur fait une Réussite Normale
- L'attaquant gagne l'attaque

Dans le cas d'une égalité, sauf les critiques :

- Le défenseur gagne sur l'attaquant en cas d'*esquive* ou de *parade*.
- Le défenseur perd sur l'attaquant en cas de *contre*.
