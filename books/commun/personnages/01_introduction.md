# Création de personnages

Bienvenue dans la création de personnages.
Vous apprendrez ici comment créer votre personnage.
En cas de doute faites appel à votre **Maître du Jeu**, MJ.
Le MJ a toujours le dernier mot, dans le cas d'une incertitude il lui revient donc de trancher.

Dans cette phase il est attendu du MJ qu'il conseille les joueurs en fonction de leur niveau de jeu et de la difficulté de jeu choisie.

## Joueurs débutants

Dans le cas de joueurs débutants il est conseillé de suivre un maximum les instructions du livre.
Il est également demandé au MJ d'aiguiller au maximum les joueurs.
Dans le cas où un joueur souhaiterait faire un **combattant** il est demandé au MJ d'aider le joueur à faire les bons choix :

- Maximiser la **Force** et la **Constitution**
- Minimiser la **Sagesse** et l'**Intelligence**
- Privilégier la population des **Êtres des océans**

Les joueurs partent avec des jets de dés prédéfinis pour les attributs.

## Joueurs moyens

Dans ce cadre il est demandé de laisser un peu plus de liberté aux joueurs.
Le MJ doit se mettre légèrement en retrait et n'aiguiller que sur demande ou s'il sent un joueur en difficulté.
Le MJ peut également insister s'il sent qu'un joueur part dans une mauvaise direction, comme un **Être de la forêt** **Reliqueur** maximisant la **Force**.
Sous l'aide du MJ les joueurs peuvent également s'amuser à croiser les classes et les populations :

- Un être mi-**Citadin** mi-**Être de la forêt**
- Un **Tirailleur** **Mage**

Les joueurs jettent les dés 6 fois d'affilée puis placent les valeurs dans les attributs qu'ils décident.

## Joueurs expérimentés

Il est demandé au MJ de laisser un maximum de liberté aux joueurs.
Les joueurs peuvent faire appel au MJ.
La liberté est laissée au MJ d'aiguiller un joueur s'il en sent le besoin.
Les joueurs sont libres de croiser les classes et les populations.

Les joueurs jettent les dés pour chaque attribut. Ils ne décident pas où vont les valeurs, le sort est décidé par les dés.
Les joueurs décident à l'avance s'ils souhaitent créer leurs personnages avant ou après les jets de dés.

## Informations supplémentaires

La liberté est laissée aux joueurs et au MJ de choisir la difficulté de jeu mais également de mixer les difficultés :

- Beaucoup de guidance du MJ avec des jets aléatoires
- Peu de guidance mais des valeurs prédéfinis

La suite du bouquin de création de personnage détaille les jets de dés, les classes, les populations et plus encore.
