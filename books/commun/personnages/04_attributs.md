# Les attributs

Il existe 6 attributs définissant les caractéristiques physique et psychologique des personnages

## Liste des attributs

### Force - FOR

- Dégâts au CAC
- Contre attaque
- Taille de l'arme

### Constitution - CON

- PV max
- Volonté
- Poids porté

### Dextérité - DEX

- Vitesse (d'attaque ou autre)
- Esquive
- Discrétion

### Intelligence - INT

- Puissance magique (dégâts des sorts)
- Déduction
- Récupération magique

### Perception - PER

- Dégâts à distance
- Observation
- Psychologie

### Sagesse - SAG

- Nombre de sorts
- Connaissance
- Persuasion

## Valeurs de départ

Des valeurs de départ d'attributs sont déterminé au début du jeu. Celles-ci peuvent être prédéfinis ou définies par la chance. En plus de ces valeurs les classes et les populations peuvent avoir un impact sur les valeurs d'attributs. Ensuite, au cours du jeu les joueurs auront la liberté de faire évoluer ces valeurs, en suivant bien évidemment quelques contraintes.

### Débutants

Si vous optez pour le mode "facile" vous pouvez utiliser 6 valeurs d'attributs par défauts à placer face aux attributs que vous souhaitez.

|     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- |
|     | 60  | 60  | 45  | 40  | 35  | 30  |

## Difficile

En mode difficile les joueurs vont déterminer les valeurs d'attributs au lancer de dés. Suivant la difficulté choisi ils peuvent:

- Faire les 6 lancés et déterminer quelle valeurs va à quel attribut, après la sélection de la classe, recommandé pour des joueurs ayant déjà effectué des jeux de rôle
- Faire les 6 lancés dans l'ordre des attributs, avant la sélection  de la classe les joueurs ne décident donc pas entièrement des caractères de leurs personnages, celà peut aiguiller les joueurs indécis et corser le jeu, recommandé pour des joueurs expérimentés en jeux de rôle
- Sélectionner la classe, la population, puis faire les 6 lancés dans l'ordre des attributs, recommandé pour les joueurs très expérimentés

Vous pouvez créer un peut toutes les règles que vous voulez ici. Attention à ne pas trop avantager ou désavantager les joueurs par rapport au scénario sélectionné.

Pour le calcul nous vous conseillons de procéder comme suit:

- 3 dés 6 multipliés par 5 pour chaque attribut, en moyenne vous atterrissez environ comme les valeurs du mode facile (45)

Ce mode peu ou non avantager les joueurs comparé au mode facile, il est conseiller de mesurer le pour et le contre avant de tenter le hasard.

## Classes et attributs

Le tableau suivant montre les bonus et malus de départ sur les attributs procurés par les classes.

|            | FOR | CON | DEX | INT | PER | SAG | *Total malus* | *Total bonus* |
| ---------- | --- | --- | --- | --- | --- | --- | ------------: | ------------: |
| Combattant | +10 | +10 |     | -5  |     | -5  |         *-10* |         *+20* |
| Mage       | -5  | -5  |     | +10 |     | +10 |         *-10* |         *+20* |
| Artificier | +5  | +5  | +5  | -5  | +10 | -10 |         *-15* |         *+25* |
| Druide     | +10 | -5  | -10 | +5  | +5  | +5  |         *-15* |         *+25* |
| Tireur     | -5  | -5  | +5  | +5  | +10 |     |         *-10* |         *+20* |
| Rôdeur     | +5  | -10 | +10 | +5  | +5  | -5  |         *-15* |         *+25* |
| Reliqueur  |     | -10 | -5  | +10 | +5  | +10 |         *-15* |         *+25* |

### Exemple

Un personnage avec les attributs de bases suivant:

| FOR | CON | DEX | INT | PER | SAG |
| --- | --- | --- | --- | --- | --- |
| 60  | 60  | 45  | 40  | 35  | 30  |

Qui est définie rôdeur aura les malus et bonus suivants:

| FOR | CON | DEX | INT | PER | SAG |
| --- | --- | --- | --- | --- | --- |
| +5  | -10 | +10 | +5  | +5  | -5  |

Ce qui donnera comme valeurs de départ de l'aventure:

|                  | FOR | CON | DEX | INT | PER | SAG |
| ---------------- | --- | --- | --- | --- | --- | --- |
| Valeurs de bases | 60  | 60  | 45  | 40  | 35  | 30  |
| Bonus de classe  | +5  | -10 | +10 | +5  | +5  | -5  |
| Valeurs finales  | 65  | 50  | 55  | 45  | 40  | 25  |
