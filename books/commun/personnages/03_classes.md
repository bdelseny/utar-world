# Les classes

Les classes définissent les caractéristiques d'un personnage.
Ces caractéristiques peuvent s'apparenter à un métier ou des capacités.
Il existe 7 classes sur la planète **Xandu**.

## Liste des classes

### Combattant

- Spécialiste en corps à corps, CAC, (Armes moyennes et grosses)
- Spécialiste en lancés (Armes petites et moyennes)
- Mauvais en combats à distance
- Pas de magie
- Tacticien

### Mage

- Faible au CAC (petites armes uniquement)
- Faible aux combats à distances et lancés
- Spécialiste de la magie :
  - Spécialiste d'un élément :
    - Eau
    - Feu
    - Terre
    - Air
- Sensible

### Artificier

- Moyen au CAC (armes moyennes uniquement)
- Faible en magie (magie pour créer les munitions uniquement)
- Bon à distance (armes moyennes et grosses)
- Bon en lancés (armes moyennes)
- Dépend des munitions pour la distance
- Mécanicien

### Druide

- Fort au CAC (Armes moyennes uniquement)
- Faible à distance et en lancés
- Fort en magie : (pas élément)
  - Type :
    - Attaque : Débuff et dégât
    - Défense : Buff et heal
    - Paladin : Dégât et heal
    - Prêtre : Débuff et buff
    - Protecteur : Buff et dégâts
    - Tourmenteur : Débuff et heal
- Lien naturel (animal de compagnie, soit aide au combat soit utilitaire)
- Ami des animaux

### Tireur

- Mauvais au CAC (petites armes uniquement)
- Faible en magie (altération des tirs, augmentation de précision, débuff)
- Spécialiste à distance (Armes petites, moyennes et grosses)
  - Dépend des munitions
- Mauvais en lancés (petites armes uniquement)
- Observateur

### Rôdeur

- Faible en magie (peu l'utilisé mais de manière modérée, pas de dégât, plus du détournement d'attention)
- Spécialiste en CAC (Armes petites et moyennes)
- Spécialiste en lancés (Armes petites)
- Spécialiste à distance (petites armes, fronde/arbalète)
  - Dépend des munitions pour la distance
- Discret

### Reliqueur

- Spécialiste en magie :
  - Spécialiste d'un élément :
    - Eau
    - Feu
    - Terre
    - Air
  - Pas de type de magie prédéfini
  - Charge ses reliques avec sa magie pour utilisation ultérieure
  - Ne peut pas utiliser la magie en dehors du chargement de relique ou seulement pour de faibles sorts
- Une spécialité parmi les suivantes :
  - Spécialiste au CAC (Relique d'arme, ex : épée de feu)
  - Spécialiste à distance (Relique de munition ou d'arme, arc à débuff ou flèche de feu)
  - Spécialiste en lancés (Relique à usage unique, ex : bombe de feu)
- Chanceux

![Reliqueur](images/relicor.png)

## Classes et populations

Le tableau ci-dessous représente la probabilité de trouver une classe au sein d'une population.
Il représente en quelque sorte la familiarité d'une classe associée à une population.

|            | Être des océans | Être de la forêt | Citadin |
| ---------- | --------------- | ---------------- | ------- |
| Combattant | +++             | -                | ++      |
| Mage       | +               | +                | ++      |
| Artificier | ++              | -                | +++     |
| Druide     | ++              | +++              | -       |
| Tireur     | +               | ++               | +       |
| Rôdeur     | -               | +++              | ++      |
| Reliqueur  | ++              | -                | +++     |
