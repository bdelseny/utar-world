# Les populations d'Utar

Dans **Les mondes d'UTAR** il existe 3 populations sur la planète **Xandu**:

- Les **Êtres des océans**
- Les **Êtres de la forêt**
- Les **Citadins**

Chacune de ces populations a des affinités avec des classes.
Ces affinités sont déterminées par la vie que sont amenées à suivre ces populations.

## Liste des populations

### Êtres des océans

Les êtres des océans sont composés de différentes factions :

- Les **Atlantes** vivants dans des cités sous-marine
- Les **Zagaschin** vivants en surface sur des cités mouvantes
- Les **Arapi** des nomades qui bougent de cités en cités à l'aide de vaisseaux sous-marin

### Êtres de la forêt

Les êtres de la forêts sont constitués de différents factions :

- Les **Noa** vivants librement dans la forêt sans lieu d'attache
- Les **Naayos** vivants de manière grégaire au sein de la forêt
- Les **Gilid** vivants en lisière de la forêt

### Citadins

Les citadins sont constitués de différentes factions :

- Les **Yatri** voyageant de cité en cité, souvent pour du commerce
- Les **Ao** vivants dans des cités aériennes
- Les **Talamh** vivants dans des cités terrestres
